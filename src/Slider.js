import React, { Component } from "react";
import 'antd/dist/antd.css';
import { Carousel } from 'antd';
import {Link} from "react-router-dom";
import {Route} from "react-router-dom";
import About from "./About";


class Slider extends Component {

    constructor(props) {
        super(props);
        this.state = {
            items: []
        };
    }

    componentDidMount() {
        fetch("http://localhost/deshiro/api/api-slider")
            .then(res => res.json())
            .then(parsedJSON => parsedJSON.values.map(data => (
                {
                    slider_id: `${data.slider_id}`,
                    slider_type: `${data.slider_type}`,
                    slider_title: `${data.slider_title}`,
                    slider_shortdesc: `${data.slider_shortdesc}`,
                    slider_image: `${data.slider_image}`,
                    slider_url: `${data.slider_url}`,
                    slider_status: `${data.slider_status}`,

                }
            )))
            .then(items => this.setState({
                items,
                isLoaded: false
            }))
            .catch(error => console.log('parsing failed', error))
    }

    render() {
        const {items } = this.state;
        const stylecarousel = {
            width: '750px'
        }
        return (
            <div className="boxWhite">
                <h2>Slider</h2>
                <Carousel autoplay draggable={true} style={stylecarousel}>
                {
                    items.length > 0 ? items.map(item => {
                        const {slider_id, slider_type, slider_title, slider_shortdesc, slider_image, slider_url, slider_status} = item;

                        var base64string = slider_image

                        var img = document.createElement("img")

                        img.setAttribute("src", base64string)
                        var imgheight = img.height
                        var imgwidth = img.width

                        const contentStyle = {
                            height: '370px',
                            color: '#fff',
                            lineHeight: '370px',
                            textAlign: 'center',
                            backgroundImage: "url(" + slider_image + ")",
                            backgroundRepeat: 'no-repeat',
                            backgroundSize: 'cover',
                            backgroundColor: '#000',
                        };

                        if(slider_type == 'text') {
                            var href = '/sliderdetail'
                            var target = '_self'
                        } else {
                            var href = slider_url
                            var target = '_blank'
                        }

                        return (
                            <div>
                                <h3 style={contentStyle}>
                                    <a href={href} target={target}>{slider_title}</a>
                                </h3>
                            </div>
                        );
                    }) : null
                }
                </Carousel>
            </div>
        );

    }
}

export default Slider;