import React, { Component } from "react";
import {
    Route,
    NavLink,
    HashRouter
} from "react-router-dom";
import Home from "./Home";
import Slider from "./Slider";
import About from "./About";



class Main extends Component {
    render() {
        return (
            <HashRouter>
                <div>
                    <h1 className="title">DESHIRO</h1>
                    <ul className="header">
                        <li><NavLink to="/">Slider</NavLink></li>

                    </ul>
                    <div className="content">
                        <Route exact path="/" component={Slider}/>
                        <Route exact path="/sliderdetail" component={About}/>
                    </div>
                </div>
            </HashRouter>
        );
    }
}

export default Main;